from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.IntegerField(unique=True)

class Appointment(models.Model):
    vin = models.CharField(max_length=17, null=True)
    customer_name = models.CharField(max_length=100)
    appointment_datetime = models.DateTimeField(auto_now=False)
    technician = models.ForeignKey(Technician, on_delete=models.PROTECT)
    reason_for_visit = models.TextField()
    is_vip = models.BooleanField(default=False)
    is_complete = models.BooleanField(default=False)
