from common.json import ModelEncoder
from .models import AutomobileVO, Appointment, Technician


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'import_href',
        'vin',
        'name',
        ]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        'name',
        'employee_number',
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'vin',
        'customer_name',
        'appointment_datetime',
        'technician',
        'reason_for_visit',
        'is_vip',
        'id',
        'is_complete',
    ]

    encoders = {"technician":TechnicianListEncoder()}