from re import T
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import AutomobileVO, Appointment, Technician
from .encoders import AutomobileVODetailEncoder, AppointmentListEncoder, TechnicianListEncoder


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {'appointments': appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            employee_number = int(content['technician'])
            technician = Technician.objects.get(
                employee_number=employee_number)
            content['technician'] = technician
        except:
            return JsonResponse(
                {"message": "Technician does not exist"}
            )

        try:
            automobiles = AutomobileVO.objects.all()
            for automobile in automobiles:
                if content['vin'] == automobile.vin:
                    content['is_vip'] = True
        except:
            return JsonResponse(
                {"message": ""}
            )

        appointments = Appointment.objects.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_vin_list(request, vin):
    vin = AutomobileVO.objects.get(vin=vin)
    api_vin_list = Appointment.objects.filter(vin=vin)
    return JsonResponse(
        {'api_vin_list': api_vin_list},
        encoder=AppointmentListEncoder,
    )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {'technicians': technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT"])
def api_edit_appointment(request, id):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Appointment.objects.filter(id=id).update(**content)
        service = Appointment.objects.get(id=id)
        return JsonResponse(
            service,
            encoder=AppointmentListEncoder,
            safe=False
        )
