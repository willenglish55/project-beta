from django.urls import path
from .api_views import api_list_appointments, api_list_technicians, api_vin_list, api_edit_appointment

urlpatterns = [
    path('appointments/', api_list_appointments, name='api_list_appointments'),
    path('appointments/<int:vin>/', api_vin_list, name='api_vin_list'),
    path('appointments/edit/<int:id>/', api_edit_appointment, name='api_edit_appointment'),
    path('technicians/', api_list_technicians, name="api_list_technicians"),
]