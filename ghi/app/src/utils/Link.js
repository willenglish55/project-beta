import React from 'react';
import { NavLink } from 'react-router-dom';

export default function Link(props) {
  return (
    <NavLink className="dropdown-item" aria-current="page" to={props.to}>{props.title}</NavLink>
  )
}
