import { NavLink } from 'react-router-dom';
import Link from './utils/Link';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="main_nav">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item dropdown p-3">
                <a className="nav-link" type="button" id="navbarDarkDropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                  Inventory
                </a>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li><Link to="inventory/manufacturer/list/" title='Manufacturers'/></li>
                  <li><Link to="inventory/models/list/" title='Vehicle Models'/></li>
                  <li><Link to="inventory/automobiles/list/" title='Automobiles'/></li>
                </ul>
            </li>
            <li className="nav-item dropdown p-3">
                <a className="nav-link" type="button" id="navbarDarkDropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                  Sales
                </a>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li><Link to="sales/list/" title='Sales Records'/></li>
                  <li><Link to="sales/customer/" title='New Customer'/></li>
                  <li><Link to="sales/employee/" title='New Employee'/></li>
                </ul>
            </li>
            <li className="nav-item dropdown p-3">
                <a className="nav-link" type="button" id="navbarDarkDropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                  Service
                </a>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li><Link to="service/list/" title='Service Appointments'/></li>
                  <li><Link to="service/history/" title='Service History'/></li>
                  <li><Link to="service/technician/" title='New Technician'/></li>
                </ul>
            </li>
          </ul>
        </div>
        
      </div>
    </nav>
  )
}

export default Nav;
