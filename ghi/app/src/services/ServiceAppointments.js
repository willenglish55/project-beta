import React from 'react'
import { useState, useEffect } from 'react';
import AppointmentForm from './AppointmentForm';


export default function ServiceAppointments() {

    const [appointments, setAppointments] = useState([]);

    const fetchAppointments = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const result = await fetch(url);
        const recordsJSON = await result.json();
        console.log(recordsJSON)
        setAppointments(recordsJSON.appointments);
    }

    useEffect(() => {
        fetchAppointments()
    }, []);

    async function deleteAppointment(id) {
        const url = `http://localhost:8080/api/appointments/edit/${id}/`;
        const result = await fetch(url, { method: 'DELETE' });
        if (result.ok) {
            setAppointments(appointments.filter((appointment) => appointment.id != id));
        }
    }

    async function completeAppointment(id) {
        const data = { is_complete: "True" }
        const url = `http://localhost:8080/api/appointments/edit/${id}/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const result = await fetch(url, fetchConfig);
        if (result.ok) {
            setAppointments(appointments.filter((appointment) => appointment.id != id));
        }

    }

    const addAppointment = (appointment) => {
        setAppointments([...appointments, appointment]);
      }

    return (
        <div className="row">
            <div className='mt-4'>
        <h1 className='p-0'>Appointments</h1>
        <div className="row justify-content-between">
          <div className="col-3 p-0">
            <div className='mb-4'></div>
            </div>
            <div className="col-3">
              <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Add Appointment
              </button>
              <AppointmentForm afterSubmit={addAppointment} />
            
          </div>
        </div>
      </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>VIP?</th>
                        <th>Progress</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments?.filter((appointment) => {
                        if (appointment.is_complete == false) {
                            return appointment;
                        }
                    }).map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{new Date(appointment.appointment_datetime).toLocaleDateString()}</td>
                                <td>{new Date(appointment.appointment_datetime).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}</td>
                                <td>{appointment.technician.name}</td>
                                <td>{appointment.reason_for_visit}</td>
                                <td>{appointment.is_vip.toString()}</td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => deleteAppointment(appointment.id)} type="button">Cancel</button>
                                    <button className="btn btn-success" onClick={() => completeAppointment(appointment.id)} type="button">Finished</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
