import React, { Component } from 'react'


export default class AppointmentForm extends Component {
    constructor() {
        super();
        this.state = {
            vin: '',
            customer_name: '',
            appointment_datetime: '',
            technician: '',
            technicians: [],
            reason_for_visit: '',
            successClass: "alert alert-success d-none",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.timer = this.timer.bind(this);
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState({ [event.target.name]: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.successClass;
        delete data.technicians;
        console.log(data);

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            console.log(newAppointment);

            const cleared = {
                vin: '',
                customer_name: '',
                appointment_datetime: '',
                technician: '',
                reason_for_visit: '',
                successClass: "alert alert-success",
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.technicians });
        }
    }

    timer() {
        setTimeout(() => {
            return this.setState({ successClass: "alert alert-success d-none" });;
        }, 4000);
    }

    render() {
        return (
            <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Add Appoinement</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={this.handleSubmit} id="create-location-form">
                                <div className="form-floating mb-3">
                                    <input value={this.state.vin} onChange={this.handleChange} placeholder="VIN" required type="int" name="vin" id="vin" className="form-control" />
                                    <label htmlFor="vin">VIN</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.customer_name} onChange={this.handleChange} placeholder="Name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                                    <label htmlFor="customer_name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.appointment_datetime} onChange={this.handleChange} placeholder="Date" required type="datetime-local" name="appointment_datetime" id="appointment_datetime" className="form-control" />
                                    <label htmlFor="appointment_datetime">Date / Time</label>
                                </div>
                                <div className="mb-3">
                                    <select value={this.state.technician} onChange={this.handleChange} placeholder="Technician" required type="text" name="technician" id="technician" className="form-control">
                                        <option value="">Technician</option>
                                        {this.state.technicians?.map(technician => {
                                            return (
                                                <option key={technician.employee_number} value={technician.employee_number}>
                                                    {technician.name}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.reason_for_visit} onChange={this.handleChange} placeholder="Oil change, inspection, etc..." required type="text" name="reason_for_visit" id="reason_for_visit" className="form-control" />
                                    <label htmlFor="reason_for_visit">Reason for visit</label>
                                </div>
                                <div className={this.state.successClass} id="success-alert">
                                    Appointment Added
                                </div>
                                <button onClick={this.timer} className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
