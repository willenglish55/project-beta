import React from 'react'
import { useState, useEffect } from 'react';
import SaleRecordForm from './SaleRecordForm';

export default function SaleRecords() {

    const [employees, setEmployees] = useState([]);

    const [employee, setEmployee] = useState([]);

    const [sales, setSales] = useState([]);

    const fetchEmployees = async () => {
        const url = 'http://localhost:8090/api/employee/';
        const result = await fetch(url);
        const recordsJSON = await result.json();
        setEmployees(recordsJSON.employees);
    }

    const fetchSales = async () => {
        const url = `http://localhost:8090/api/sales/`;
        const result = await fetch(url);
        const recordsJSON = await result.json();
        setSales(recordsJSON.sales);
    }


    useEffect(() => {
        fetchEmployees();
        fetchSales();
    }, []);

    const addSale=(sale) => {
        setSales([...sales, sale]);
    }

    return (
        <div className="row">
            <div className="mt-4">
                <h1 className='p-0'>Sales Records</h1>
                <div className="row justify-content-between">
                    <div className="col-3 p-0">
                            <div className="mb-4">
                                <select className="form-select input-small" aria-label="Default select example" onChange={(event) => {
                                    setEmployee(event.target.value);
                                }}
                                    required
                                    name="employee"
                                    id="employee">
                                    <option value="">All Employees</option>
                                    {employees?.map(employee => {
                                        return (
                                            <option key={employee.number} value={employee.number}>{employee.name}</option>
                                        )
                                    })
                                    }
                                </select>
                        </div>
                    </div>
                    <div className="col-3">
                        <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                            Create New Record
                        </button>
                        <SaleRecordForm afterSubmit={addSale}/>
                    </div>
                </div>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee</th>
                        <th>Customer</th>
                        <th>Vin</th>
                        <th>Sales Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales?.filter((val) => {
                        if (val.employee.number == employee) {
                            return val;
                        } else if (employee == '') {
                            return val;
                        }
                    }).map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.employee.name}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.sales_price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
