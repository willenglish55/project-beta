import React, { Component } from 'react'

export default class PotentialCustomerForm extends Component {
    constructor() {
        super();
        this.state = {
            name: '',
            address: '',
            phone: '',
            successClass: "alert alert-success d-none",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.timer = this.timer.bind(this);
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState({ [event.target.name]: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.successClass;
        console.log(data);

        const customerUrl = 'http://localhost:8090/api/customer/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer);

            const cleared = {
                name: '',
                address: '',
                phone: '',
                successClass: "alert alert-success",
            };
            this.setState(cleared);
        }
    }

    timer() {
        setTimeout(() => {
            return this.setState({ successClass: "alert alert-success d-none" });;
        }, 4000);
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Customer</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name} onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="Name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.address} onChange={this.handleChange} placeholder="address" required type="test" name="address" id="address" className="form-control" />
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.phone} onChange={this.handleChange} placeholder="Phone Number" required type="int" name="phone" id="phone" className="form-control" />
                                <label htmlFor="phone">Phone Number</label>
                            </div>
                            <div className={this.state.successClass} id="success-alert">
                                Customer Added.
                            </div>
                            <button onClick={this.timer} className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
    )}
}
