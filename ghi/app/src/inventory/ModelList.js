import React from 'react';
import { useState, useEffect } from 'react';
import ModelForm from './ModelForm';

export default function ModelList() {

  const [models, setModels] = useState([]);

  const fetchModels = async () => {
    const url = 'http://localhost:8100/api/models/';
    const result = await fetch(url);
    const recordsJSON = await result.json();
    setModels(recordsJSON.models);
  }

  useEffect(() => {
    fetchModels()
  }, []);

  const addModel = (model) => {
    setModels([...models, model]);
  }

  return (
    <div className="row">
      <div className='mt-4'>
        <h1 className='p-0'>Models</h1>
        <div className="row justify-content-between">
          <div className="col-3 p-0">
            <div className='mb-4'></div>
            </div>
            <div className="col-3">
              <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Add Model
              </button>
              <ModelForm afterSubmit={addModel} />
            
          </div>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models?.map(model => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td className="photo">
                  <img className="photo" src={model.picture_url} />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}
