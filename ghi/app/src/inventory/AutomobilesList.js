import React from 'react';
import { useState, useEffect } from 'react';
import AutomobilesForm from './AutomobilesForm';

export default function AutomobilesList() {

  const [automobiles, setAutomobiles] = useState([]);

  const fetchAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const result = await fetch(url);
    const recordsJSON = await result.json();
    setAutomobiles(recordsJSON.autos);
  }

  useEffect(() => {
    fetchAutomobiles()
  }, []);

  const addAutomobile = (automobile) => {
    setAutomobiles([...automobiles, automobile]);
  }

  return (
    <div className="row">
      <div className='mt-4'>
        <h1 className='p-0'>Automobiles</h1>
        <div className="row justify-content-between">
          <div className="col-3 p-0">
            <div className='mb-4'></div>
            </div>
            <div className="col-3">
              <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Add Automobile
              </button>
              <AutomobilesForm afterSubmit={addAutomobile} />
            
          </div>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {automobiles?.map(automobile => {
            return (
              <tr key={automobile.id}>
                <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>

              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}
