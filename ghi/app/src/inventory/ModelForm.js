import React, { Component } from 'react'


export default class ModelForm extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      manufacturer_id: '',
      successClass: "alert alert-success d-none",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.timer = this.timer.bind(this);
  }

  handleChange(event) {
    const value = event.target.value;
    this.setState({ [event.target.name]: value })
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.successClass;
    delete data.manufacturers;
    console.log(data);

    const modelUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      const newModel = await response.json();
      console.log(newModel);
      this.props.afterSubmit(newModel);

      const cleared = {
        name: '',
        manufacturer_id: '',        
        successClass: "alert alert-success",
      };
      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        this.setState({ manufacturers: data.manufacturers });
    }
}

  timer() {
    setTimeout(() => {
      return this.setState({ successClass: "alert alert-success d-none" });;
    }, 4000);
  }

  render() {
    return (
      <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">New Manufacturer</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
            <form onSubmit={this.handleSubmit}>
              <div className="form-floating mb-3">
                <input value={this.state.name} onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="Name">Name</label>
              </div>
              <div className="mb-3">
                <select value={this.state.id} onChange={this.handleChange} placeholder="Manufacturer ID" required type="number" name="manufacturer_id" id="id" className="form-control">
                  <option value="">Manufacturer</option>
                  {this.state.manufacturers?.map(manufacturer => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className={this.state.successClass} id="success-alert">
                Model Added.
              </div>
              <button onClick={this.timer} className="btn btn-primary" data-bs-dismiss="modal">Create</button>
            </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
