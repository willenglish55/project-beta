import React from 'react';
import { useState, useEffect } from 'react';
import ManufacturerForm from './ManufacturerForm';

export default function ManufacturerList() {

  const [manufacturers, setManufacturers] = useState([]);

  const fetchManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const result = await fetch(url);
    const recordsJSON = await result.json();
    setManufacturers(recordsJSON.manufacturers);
  }

  useEffect(() => {
    fetchManufacturers()
  }, []);

  const addManfacturer = (manufacturer) => {
    setManufacturers([...manufacturers, manufacturer]);
  }

  return (
    <div className="row">
      <div className='mt-4'>
        <h1 className='p-0'>Manufacturers</h1>
        <div className="row justify-content-between">
          <div className="col-3 p-0">
            <div className='mb-4'></div>
            </div>
            <div className="col-3">
              <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Add Manufacturer
              </button>
              <ManufacturerForm afterSubmit={addManfacturer} />
            
          </div>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers?.map(manufacturer => {
            return (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}
