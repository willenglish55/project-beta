import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SaleRecords from './sales/SaleRecords';
import SaleRecordForm from './sales/SaleRecordForm';
import EmployeeForm from './sales/EmployeeForm';
import PotentialCustomerForm from './sales/PotentialCustomerForm';
import ServiceAppointments from './services/ServiceAppointments'
import ServiceHistory from './services/ServiceHistory';
import AppointmentForm from './services/AppointmentForm';
import TechnicianForm from './services/TechnicianForm';
import ManufacturerForm from './inventory/ManufacturerForm';
import ManufacturerList from './inventory/ManufacturerList';
import ModelList from './inventory/ModelList';
import ModelForm from './inventory/ModelForm';
import AutomobilesList from './inventory/AutomobilesList';
import AutomobilesForm from './inventory/AutomobilesForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="sales">
            <Route path="list" element={<SaleRecords />} />
            <Route path="new" element={<SaleRecordForm />} />
            <Route path="customer" element={<PotentialCustomerForm />} />
            <Route path="employee" element={<EmployeeForm />} />
          </Route>
          <Route path="service">
            <Route path="list" element={<ServiceAppointments />} />
            <Route path="history" element={<ServiceHistory />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="technician" element={<TechnicianForm />} />
          </Route>
          <Route path="inventory">
            <Route path="manufacturer">
              <Route path="list" element={<ManufacturerList />} />
              <Route path="new" element={<ManufacturerForm />} />
            </Route>
            <Route path="models">
              <Route path="list" element={<ModelList />} />
              <Route path="new" element={<ModelForm />} />
            </Route>
            <Route path="automobiles">
              <Route path="list" element={<AutomobilesList />} />
              <Route path="new" element={<AutomobilesForm />} />
            </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
