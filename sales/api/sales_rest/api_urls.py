from django.urls import path
from .api_views import (
    api_list_sales,
    api_create_customer,
    api_create_employee,
    api_list_available
)

urlpatterns = [
    path('sales/', api_list_sales ,name="api_list_sales"),
    path('customer/', api_create_customer, name="api_create_customer"),
    path('employee/', api_create_employee, name="api_create_employee"),
    path('automobiles/available/', api_list_available, name="api_list_available")
]