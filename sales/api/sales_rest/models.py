from django.db import models

class Employee(models.Model):
    name = models.CharField(max_length=150)
    number = models.IntegerField()

    def __str__(self):
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length=150)
    address = models.TextField()
    phone = models.IntegerField()

    def __str__(self):
        return self.name

class AutomobileVO(models.Model):
    import_href = models.URLField(max_length=200, unique=True)
    vin = models.IntegerField(null=True)
    name = models.CharField(max_length=150, null=True)

    def __str__(self):
        return f'{self.vin}'

class SalesRecord(models.Model):
    employee = models.ForeignKey(
        Employee,
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        on_delete=models.PROTECT,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales_record",
        on_delete=models.PROTECT,
        unique=True,
    )
    sales_price = models.FloatField()

    def __str__(self):
        return f'{self.customer.name} - {self.automobile.vin}'