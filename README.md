# CarCar

Team:

* Drew Saenz - Service
* Will English - Sales

## Design

## Service microservice

My models were 'AutomobileVO', 'Technician', and 'Appointment'. 'Technician' was the least involved. 
I only made a list view for methods "GET" and "POST" for that. The 'Appointment model most integrated 
with the inventory microservice by cross-referencing vins in the inventory with the ones in our appointment list.

## Sales microservice

The models for sales conatain the necesarry elements and fields layed out in the problem scope.
The poller for the sales polls for the automobiles from the inventory and creates automobile value objects
in the sales microservice. These automobiles get listed for when a new sale record is created but only the
automobiles that have not been apart of a sales record are listed. For the sales records the models are 
set up so that the sales records are not deleted when an employee is deleted because even though that 
employee no longer works there, the record of the sale needs to persist.